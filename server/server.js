const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const uuid = require('uuid');

const port = 8080;

const allRooms = {};

app.get('/');

// connect to socket
io.on('connection', (socket) => {

  const MESSAGE = 'message',
    JOINED_TO_ROOM = 'joined to room',
    LEAVE_ROOM = 'leave room';

  let addedUser = false;
  let userRooms = [];

  // when user connect to chat
  socket.on('login', ({username, userRoom, rooms, userKey}) => {

    if (addedUser) return;

    // set socket to store username and create user room
    socket.username = username;
    addedUser = true;
    socket.room = userRoom !== null && userRoom ? userRoom : uuid(4);
    socket.userkey = (userKey !== 'undefined' && userKey !== null) ? userKey : uuid(4);
    if (!rooms || rooms === null) {
      userRooms.push(socket.room);
      socket.join(socket.room);
    }
    if (rooms && rooms !== null) {
      rooms.split(',').map(room => {
        if (room !== socket.room) {
          socket.join(room);
          userRooms.push(room);
          if (allRooms[ room ] && allRooms[ room ].users.find(user => user.userkey !== socket.userkey)) {
            allRooms[ room ].users.push({ username: socket.username, socketId: socket.id, userkey: socket.userkey });
          }
        }
      });
    }


    // create new prop in the store of rooms or add user
    if (!allRooms[ socket.room ]) allRooms[ socket.room ] = {
      users: [],
      roomName: socket.username + '\'s chat'
    };
    allRooms[ socket.room ].users.push({ username: socket.username, socketId: socket.id, userkey: socket.userkey });

    //whet user join to chat(aren't using now and need refactor client side for this)
    socket.broadcast.to(socket.room).emit('user joined', ({
      username: socket.username,
      room: socket.room,
      type: JOINED_TO_ROOM
    }));

    // emit user room for set it on client side, and send update room-list
    socket.emit('user room', {userRoom: socket.room, userKey: socket.userkey});
    socket.broadcast.emit('rooms', allRooms);
    socket.emit('rooms', allRooms);
  });

  // when user try to join room
  socket.on('join room', (room) => {
    // if selected room is non-existing, then send failed connection and redirect to user room
    if (!allRooms[ room ]) {
      socket.emit('failed connect to room', socket.room);
      return;
    }
    if (allRooms[ room ].users.find(user => user.userkey === socket.userkey)) {
      return;
    }
    socket.join(room);
    socket.broadcast.to(room).emit('user joined', ({
      username: socket.username,
      room: room,
      type: JOINED_TO_ROOM
    }));
    allRooms[ room ].users.push({ username: socket.username, socketId: socket.id, userkey: socket.userkey });

    // after join and change information in rooms, update room-list for users
    socket.broadcast.emit('rooms', allRooms);
    socket.emit('rooms', allRooms);
  });

  // when user send new message
  socket.on('new message', ({room, text}) => {
    let time = (new Date()).toLocaleTimeString();
    const message = {
      username: socket.username,
      room: room,
      text: text,
      time: time,
      type: MESSAGE,
      userkey: socket.userkey
    };
    socket.broadcast.to(room).emit('new message', message);
    socket.emit('new message', message);
  });

  // when user leave room, it isn't realized on client side now
  socket.on('leave room', (room) => {
    socket.leave(room, () => {
      socket.broadcast.to(room).emit('user joined', ({
        username: socket.username,
        room: room,
        type: LEAVE_ROOM
      }));
    });
    allRooms[ room ].users = allRooms[ room ].users.filter(user => user.userkey !== socket.userkey);
    deleteEmptyRoom();
    userRooms = userRooms.filter(userRoom => userRoom !== room);
    if (userRooms.length === 0) {
      addedUser = false;
    }
    socket.broadcast.emit('rooms', allRooms);
    socket.emit('rooms', allRooms);

  });

  // when user disconnected, user leave rooms and notife all users
  socket.on('disconnect', () => {
    if (addedUser) {
      Object.keys(allRooms).map(room => {
        socket.leave(room, () => {
          socket.broadcast.to(room).emit('user joined', ({
            username: socket.username,
            room: room,
            type: LEAVE_ROOM
          }));
        });
        allRooms[ room ].users = allRooms[ room ].users.filter(user => user.userkey !== socket.userkey);
      });

      // check empty rooms
      deleteEmptyRoom();

      socket.broadcast.emit('rooms', allRooms);
      socket.emit('rooms', allRooms);
    }
  });
});

http.listen(port, () => console.log(`server created and listen port: ${ port }`));

// if rooms is empty then it will be delete from room-list
const deleteEmptyRoom = () => {
  Object.keys(allRooms).map(room => {
    if (allRooms[ room ].users.length === 0) {
      delete allRooms[ room ];
    }
  });
};
