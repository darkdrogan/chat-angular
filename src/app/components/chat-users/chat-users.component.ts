import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat-users',
  templateUrl: './chat-users.component.html',
  styleUrls: ['./chat-users.component.scss']
})
export class ChatUsersComponent implements OnInit {
  @Input() currentRoom: string;
  @Input() users;

  constructor() {
  }

  ngOnInit() {
  }

}
