import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent {
  userName = '';
  @Output() submitUsername = new EventEmitter<string>();

  constructor() {
  }

  login() {
    const username = this.userName.trim();
    if (username.length > 0) {
      this.submitUsername.emit(username);
      this.userName = '';
    }
  }
}
