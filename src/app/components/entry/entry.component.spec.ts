import { EntryComponent } from './entry.component';

describe('EntryComponent tests', () => {
  let entryComponent: EntryComponent = null;

  beforeEach(() => {
    entryComponent = new EntryComponent();
  });

  it('should set instance correctly', () => {
    expect(entryComponent).not.toBeNull();
  });

  it('should start with empty username field', function () {
    expect(entryComponent.userName).toEqual('');
  });

  it('should clear username field after submit', function () {
    entryComponent.userName = 'another';
    entryComponent.login();
    expect(entryComponent.userName).toEqual('');
  });
});
