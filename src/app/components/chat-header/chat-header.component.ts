import { Component, ElementRef, HostBinding, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-chat-header',
  templateUrl: './chat-header.component.html',
  styleUrls: ['./chat-header.component.scss']
})
export class ChatHeaderComponent implements OnInit {
  protected isOverlay = false;
  protected isUsersMenu = false;
  protected isRoomsMenu = false;
  @ViewChild('fallenMenu') userMenu: ElementRef;

  constructor() { }

  ngOnInit() {
  }

  toggleUserMenu() {
    this.isOverlay = !this.isOverlay;
    this.isUsersMenu = !this.isUsersMenu;
  }

  toggleRoomsMenu() {
    this.isOverlay = !this.isOverlay;
    this.isRoomsMenu = !this.isRoomsMenu;
  }
}
