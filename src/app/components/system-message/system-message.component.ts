import { Component, Input, OnInit } from '@angular/core';
import { SystemMessage } from '../../models/system-message';

@Component({
  selector: 'app-system-message',
  templateUrl: './system-message.component.html',
  styleUrls: ['./system-message.component.scss']
})
export class SystemMessageComponent implements OnInit {
  @Input('dataForSysMessage') message: SystemMessage;

  constructor() { }

  ngOnInit() {
  }

}
