import { ChatRoomsComponent } from './chat-rooms.component';

describe('ChatRoomsComponent tests', () => {
  let chatRoomsComponent: ChatRoomsComponent = null;

  beforeEach(() => chatRoomsComponent = new ChatRoomsComponent());

  it('should set instance correctly', () => {
    expect(chatRoomsComponent).not.toBeNull();
  });

  it('should not be null rooms', () => {
    expect(chatRoomsComponent.rooms).not.toBeNull();
  });
});
