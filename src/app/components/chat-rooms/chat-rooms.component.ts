import { Component, Input, OnInit } from '@angular/core';

import { Room } from '../../models/room';

@Component({
  selector: 'app-chat-rooms',
  templateUrl: './chat-rooms.component.html',
  styleUrls: ['./chat-rooms.component.scss']
})
export class ChatRoomsComponent implements OnInit {
  @Input() rooms: Room[];

  constructor() {
  }

  ngOnInit() {
  }

}
