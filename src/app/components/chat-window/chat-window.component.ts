import { Component, DoCheck, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

import { Message } from '../../models/message';
import { SocketService } from '../../services/socket.service';
import { ServerConstApi } from '../../models/server-const-api';

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss']
})
export class ChatWindowComponent implements OnInit, DoCheck {
  @ViewChild('messageWindow') messageWindow: ElementRef;
  @Input() messages: Message[];
  @Input() currentRoom: string;
  @Input() user: string;
  messageField: string;

  constructor(private socketService: SocketService) {
  }

  ngOnInit() {
    this.scrollToBottom();
  }

  ngDoCheck(): void {
    this.scrollToBottom();
  }

  scrollToBottom() {
    this.messageWindow.nativeElement.scrollTop = this.messageWindow.nativeElement.scrollHeight;
  }

  sendMessage() {
    const message = {room: this.currentRoom, text: this.messageField};
    this.socketService.emit(ServerConstApi.MESSAGE, message);
    this.messageField = '';
  }
}
