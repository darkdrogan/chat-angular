import { ChatWindowComponent } from './chat-window.component';
import { SocketService } from '../../services/socket.service';

describe('tests for chat window component', () => {
  let chatWindowComponent: ChatWindowComponent = null;
  let socketService: SocketService = null;

  beforeEach(() => {
    socketService = new SocketService();
    chatWindowComponent = new ChatWindowComponent(socketService);
  });

  it('should set instance correctly', () => {
    expect(chatWindowComponent).not.toBeNull();
  });

  it('should clear text-field after submit', () => {
    chatWindowComponent.messageField = 'bla bla bla';
    chatWindowComponent.currentRoom = 'currentRoom';
    chatWindowComponent.sendMessage();
    expect(chatWindowComponent.messageField).toBe('');
  });
});
