import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatHeaderComponent } from './components/chat-header/chat-header.component';
import { SharedModule } from './shared/shared.module';
import { ChatModule } from './pages/chat/chat.module';

@NgModule({
  declarations: [
    AppComponent,
    ChatHeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ChatModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
