export enum ServerConstApi {
  CONNECTION = 'connection',
  LOGIN = 'login',
  MESSAGE = 'new message',
  JOIN_TO_ROOM = 'join room',
  LEAVE_ROOM = 'leave room',
  USER_JOINED = 'user joined',
  DISCONNECTION = 'disconnection',
  UPDATED_ROOMS = 'rooms',
  USER_ROOM = 'user room'
}
