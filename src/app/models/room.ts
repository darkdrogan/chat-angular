export interface Room {
  roomId: string;
  roomName: string;
  users?: {socketId: string, userkey: string, username: string}[];
}
