import { MessageType } from './message';

export interface SystemMessage {
  username: string;
  type: MessageType;
}
