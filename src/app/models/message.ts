export enum MessageType {
  MESSAGE = 'message',
  JOINED_TO_ROOM = 'joined to room',
  LEAVE_ROOM = 'leave room'
}

export interface Message {
  type?: MessageType;
  username: string;
  text?: string;
  time: string;
  room: string;
  userkey?: string;
}
