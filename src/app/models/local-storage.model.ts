export interface LocalStorageModel {
  username: string;
  userRoom?: string;
  rooms?: string;
  userKey?: string;
}
