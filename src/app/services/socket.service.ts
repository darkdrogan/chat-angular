import { Injectable } from '@angular/core';
import io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket;

  constructor() {
    this.socket = io.connect('http://localhost:8080');
    this.socket.on('connection', () => this.connected());
    this.socket.on('disconnect', () => this.disconnected());
  }

  emit(channel, message) {
    this.socket.emit(channel, message);
  }

  on(event_type): Observable<any> {
    return new Observable(observer => {
      this.socket.on(event_type, (data) => {
        if (event_type === 'error') {
          observer.error(data);
        }
        if (event_type === 'disconnect') {
          observer.complete();
        }
        observer.next(data);
      });
    });
  }

  connected() {
    this.socket.connect();
    // todo: проверяем, висит ли модалка, и если да - закрываем модалку, возвращаем юзера в разговор
  }

  disconnected() {
    this.socket.disconnect();
    // todo: бросаем модалку, что соединение потерянно, и переводим на стартовую страницу с экраном ошибкой
  }
}
