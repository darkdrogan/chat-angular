import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class LocalStorageService {

  loadLocalStorage () {
    const username = localStorage.getItem('username');
    const userRoom = localStorage.getItem('userRoom');
    const roomsId = localStorage.getItem('roomsId');
    const userKey = localStorage.getItem('userKey');
    return ({username, userRoom, rooms: roomsId, userKey});
  }

  saveUserKey (userKey) {
    localStorage.setItem('userKey', userKey);
  }

  saveUsername (username) {
    localStorage.setItem('username', username);
  }

  saveUserRoom (userRoom) {
    localStorage.setItem('userRoom', userRoom);
  }

  saveRooms (roomsId) {
    localStorage.setItem('roomsId', roomsId);
  }

  clearStorage () {
    localStorage.clear();
  }
}
