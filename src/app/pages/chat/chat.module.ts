import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './container/chat/chat.component';
import { PageChatResolver } from './container/chat/chat.resolver';
import { EntryComponent } from '../../components/entry/entry.component';
import { ChatWindowComponent } from '../../components/chat-window/chat-window.component';
import { ChatUsersComponent } from '../../components/chat-users/chat-users.component';
import { ChatVideoBroadcastingComponent } from '../../components/chat-video-broadcasting/chat-video-broadcasting.component';
import { ChatRoomsComponent } from '../../components/chat-rooms/chat-rooms.component';
import { MessageComponent } from '../../components/message/message.component';
import { SystemMessageComponent } from '../../components/system-message/system-message.component';

@NgModule({
  imports: [
    CommonModule,
    ChatRoutingModule,
    FormsModule,
  ],
  providers: [PageChatResolver],
  declarations: [
    ChatComponent,
    EntryComponent,
    ChatWindowComponent,
    ChatUsersComponent,
    ChatVideoBroadcastingComponent,
    ChatRoomsComponent,
    MessageComponent,
    SystemMessageComponent
  ]
})
export class ChatModule { }
