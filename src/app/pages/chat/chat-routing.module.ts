import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageChatResolver } from './container/chat/chat.resolver';
import { ChatComponent } from './container/chat/chat.component';

const routes: Routes = [
  {
    path: ':id',
    resolve: {chatLocalData: PageChatResolver},
    component: ChatComponent
  },
  {
    path: '',
    // resolve: {chatLocalData: PageChatResolver},
    redirectTo: '/\'',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
