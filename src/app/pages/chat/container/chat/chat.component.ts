import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Subject } from 'rxjs';
import { pluck, take, takeUntil } from 'rxjs/operators';

import { SocketService } from '../../../../services/socket.service';
import { Room } from '../../../../models/room';
import { ServerConstApi } from '../../../../models/server-const-api';
import { Message } from '../../../../models/message';
import { LocalStorageModel } from '../../../../models/local-storage.model';
import { LocalStorageService } from '../../../../services/local-storage.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
  isLogin: boolean;
  _username: string;
  _userRoom: string;
  currentRoom: string;
  ngUnsubscribe$: Subject<any> = new Subject<any>();
  _rooms: Room[];
  users = {};
  messages: Message[];
  messages$;
  private _userKey: string;

  set username(username: string) {
    this.localStorageService.saveUsername(username);
    this._username = username;
  }

  get username() {
    return this._username;
  }

  set userRoom(userRoom: string) {
    this.localStorageService.saveUserRoom(userRoom);
    this._userRoom = userRoom;
  }

  get userRoom() {
    return this._userRoom;
  }

  set rooms(rooms: Room[]) {
    this._rooms = [...rooms];
    const roomsIDs = rooms.map(room => room.roomId).join();
    this.localStorageService.saveRooms(roomsIDs);
  }

  get rooms() {
    return this._rooms;
  }

  set userKey(userKey: string) {
    this._userKey = userKey;
    this.localStorageService.saveUserKey(userKey);
  }

  get userKey() {
    return this._userKey;
  }

  constructor(
    private socketService: SocketService,
    private route: ActivatedRoute,
    private router: Router,
    private localStorageService: LocalStorageService
  ) {
  }

  ngOnInit() {
    this.route.data.pipe(take(1), pluck('chatLocalData'))
      .subscribe(
        (data: LocalStorageModel) => {
          if (data) {
            const {username, userRoom, rooms, userKey} = data;
            if (userRoom !== null && rooms !== null) {
              this.socketService.emit(ServerConstApi.LOGIN, {username, userRoom, rooms, userKey});
              this.isLogin = true;
            } else {
              this.socketService.emit(ServerConstApi.LOGIN, {username});
              this.isLogin = true;
            }
          }
        }
      );
    this.route.params.subscribe(
      ({id}) => {
        this.currentRoom = id;
        this.socketService.emit(ServerConstApi.JOIN_TO_ROOM, id);
      }
    );
    this.socketService.on(ServerConstApi.USER_ROOM).pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe((data) => {
        const {userRoom, userKey} = data;
        this.userRoom = userRoom;
        this.userKey = userKey;
      });
    this.socketService.on(ServerConstApi.UPDATED_ROOMS).pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(
        (rooms: Room[]) => {
          if (this.currentRoom && rooms[this.currentRoom]) {
            this.socketService.emit(ServerConstApi.JOIN_TO_ROOM, this.currentRoom);
            this.router.navigate(['/', this.currentRoom]);
          } else if (this.userRoom) {
            this.currentRoom = this.userRoom;
            this.router.navigate(['/', this.userRoom]);
          }
          this.rooms = Object.keys(rooms).map(room => {
            this.users[room] = rooms[room].users.map(user => user.username);
            return {roomId: room, roomName: rooms[room].roomName};
          });
        }
      );
    this.messages$ = merge(this.socketService.on(ServerConstApi.USER_JOINED), this.socketService.on(ServerConstApi.MESSAGE));
    this.messages$.pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(
        (message: Message) => {
          this.messages ? this.messages.push(message) : this.messages = [message];
        }
      );
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  login(username: string) {
    this.isLogin = true;
    this.username = username;
    this.socketService.emit(ServerConstApi.LOGIN, {username});
  }
}
