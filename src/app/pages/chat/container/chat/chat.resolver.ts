import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../../../services/local-storage.service';
import { LocalStorageModel } from '../../../../models/local-storage.model';

@Injectable()
export class PageChatResolver implements Resolve<any> {
  localData: LocalStorageModel;

  constructor(private localStorageService: LocalStorageService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    // this.localStorageService.clearStorage();
    this.localData = this.localStorageService.loadLocalStorage();
    if (this.localData.username !== null) {
      return this.localData;
    }
    return undefined;
  }
}
